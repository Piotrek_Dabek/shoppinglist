/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry, Text} from 'react-native';
import App from './out/App'
import {name as appName} from './app.json';

Text.allowFontScaling = false

AppRegistry.registerComponent(appName, () => App);
