import React from "react";
import { FlatList, Text, View } from "react-native";
import { ComponentBase } from "resub";
import { AddItemModal } from "../Components/AddItemModal";
import { ItemCard } from "../Components/ItemCard";
import { ViewBase } from "../Components/ViewBase";
import { ShoppingItemModel } from "../Model/ShoppingItemModel";
import { ShoppingListModel } from "../Model/ShoppingListModel";
import { Colors } from "../Resources/Colors";

interface ListContentScreenState {
    items: ShoppingItemModel[];
    showModal: boolean;
}

interface ListContentScreenProps extends React.Props<any> {
    shoppingList: ShoppingListModel;
    navigation?: any;
}

export class ListContentScreen extends ComponentBase<ListContentScreenProps, ListContentScreenState> {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.state.params.shoppingList.name,
    })

    protected _buildState(props: ListContentScreenProps) {
        return {
            items: props.navigation.getParam("shoppingList").getItems(),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            showModal: false,
        };
    }

    render() {
        return (
            <ViewBase onFABPressed={this.props.navigation.getParam("shoppingList").getArchived()
             ? null : this.onFABPressed}>
                <FlatList
                    data={this.state.items}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    ListEmptyComponent={this.renderEmpty}
                    ListFooterComponent={<View style={{ height: 70 }} />}
                />
                <AddItemModal isVisible={this.state.showModal} onDismiss={
                    () => this.setState({ showModal: false })} list={this.props.navigation.getParam("shoppingList")} />
            </ViewBase>
        );
    }

    private keyExtractor(item: ShoppingItemModel): string {
        return item.getTimestamp().toString();
    }

    private renderEmpty = () => {
        if (this.props.navigation.getParam("shoppingList").getArchived()) {
            return (
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 18, color: Colors.primaryText }}>This list is archived.</Text>
                </View>
            );
        } else {
            return (
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 18, color: Colors.primaryText }}>Press Add button to add first item!</Text>
                </View>
            );
        }
    }

    private renderItem({ item }): JSX.Element {
        return (<ItemCard item={item} />);
    }

    private onFABPressed = () => {
        this.setState({ showModal: true });
    }
}
