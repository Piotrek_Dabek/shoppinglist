import React from "react";
import { BackHandler, FlatList, Text, View, ViewStyle } from "react-native";
import * as Animatable from "react-native-animatable";
import { ComponentBase } from "resub";
import { AddListModal } from "../Components/AddListModal";
import { IconButton } from "../Components/IconButton";
import { ListCard } from "../Components/ListCard";
import { ViewBase } from "../Components/ViewBase";
import { ShoppingListModel } from "../Model/ShoppingListModel";
import { ShoppingListStore } from "../Model/ShoppingListStore";
import { Colors } from "../Resources/Colors";

interface ListsScreenState {
    lists: ShoppingListModel[];
    archivedLists: ShoppingListModel[];
    showModal: boolean;
}

interface ListsScreenProps extends React.Props<any> {
    navigation?: any;
}

export class ListsScreen extends ComponentBase<ListsScreenProps, ListsScreenState> {
    private didFocusSubscription: any;
    private willBlurSubscription: any;

    private currentLists: any;
    private archivedLists: any;

    static navigationOptions = ({ navigation }) => ({
        title: navigation.state.params.isArchived ? "Archived lists" : "My lists",
        headerRight: navigation.state.params.getHeaderButton ?
            navigation.state.params.getHeaderButton(navigation.state.params.isArchived) : null,
    })

    protected _buildState() {
        return {
            lists: ShoppingListStore.getInstance().getActiveLists(),
            archivedLists: ShoppingListStore.getInstance().getArchivedLists(),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            lists: [],
            archivedLists: [],
            showModal: false,
        };
        ShoppingListStore.getInstance().loadLists();
    }

    componentDidMount() {
        this.props.navigation.setParams({ getHeaderButton: this.headerButton });
        this.didFocusSubscription = this.props.navigation.addListener("didFocus", _ =>
            BackHandler.addEventListener("hardwareBackPress", this.onBackButtonPressed),
        );

        this.willBlurSubscription = this.props.navigation.addListener("willBlur", _ =>
            BackHandler.removeEventListener("hardwareBackPress", this.onBackButtonPressed),
        );
    }

    componentWillUnmount() {
        // tslint:disable-next-line: no-unused-expression
        this.didFocusSubscription && this.didFocusSubscription.remove();
        // tslint:disable-next-line: no-unused-expression
        this.willBlurSubscription && this.willBlurSubscription.remove();
    }

    render() {
        return (
            <ViewBase onFABPressed={this.props.navigation.state.params.isArchived ? null : this.onFABPressed}>
                <Animatable.View ref={ref => this.currentLists = ref} >
                    <FlatList
                        data={this.state.lists}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        ListFooterComponent={<View style={{ height: 70 }} />}
                    />
                </Animatable.View>

                <Animatable.View ref={ref => { this.archivedLists = ref; }}
                    animation="bounceOutRight" duration={1} style={this.styleFlatlist()}>
                    <FlatList
                        data={this.state.archivedLists}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        ListFooterComponent={<View style={{ height: 70 }} />}
                    />
                </Animatable.View>
                <AddListModal isVisible={this.state.showModal} onDismiss={() => this.setState({ showModal: false })} />
            </ViewBase>
        );
    }

    private keyExtractor(item: ShoppingListModel): string {
        return item.getTimestamp().toString();
    }

    private renderEmpty = () => {
        if (this.props.navigation.state.params.isArchived) {
            return (
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 18, color: Colors.primaryText }}>No archived lists.</Text>
                </View>
            );
        } else {
            return (
                <View style={{ justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 18, color: Colors.primaryText }}>
                        Press Add button to add Your first list!</Text>
                </View>
            );
        }
    }

    private renderItem = ({ item }): JSX.Element => {
        return (<ListCard list={item} navigation={this.props.navigation} />);
    }

    private onFABPressed = () => {
        this.setState({ showModal: true });
    }

    private headerButton = (isArchived: boolean) => {
        return (<IconButton onPress={() => {
            isArchived ? this.currentLists.bounceInLeft(800, 300) : this.currentLists.bounceOutLeft(800);
            isArchived ? this.archivedLists.bounceOutRight(800) : this.archivedLists.bounceInRight(800, 300);
            this.props.navigation.setParams({ isArchived: !isArchived });
        }}
            icon={isArchived ? "paper" : "archive"}
            style={{ marginRight: 10, backgroundColor: "transparent", width: 30, height: 30 }}
        />);
    }

    private onBackButtonPressed = () => {
        if (this.props.navigation.state.params.isArchived) {
            this.props.navigation.setParams({ isArchived: false });
            this.currentLists.bounceInLeft(800, 300);
            this.archivedLists.bounceOutRight(800);
            return true;
        } else {
            return false;
        }
    }

    private styleFlatlist(): ViewStyle {
        return {
            position: "absolute",
            top: 20,
            left: 20,
            width: "100%",
        };
    }
}
