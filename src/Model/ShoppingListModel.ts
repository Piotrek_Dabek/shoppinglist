import { Alert } from "react-native";
import { autoSubscribe, AutoSubscribeStore, StoreBase } from "resub";
import { ShoppingItemModel } from "./ShoppingItemModel";
import { ShoppingListStore } from "./ShoppingListStore";

@AutoSubscribeStore
export class ShoppingListModel extends StoreBase {
    private name: string;
    private timestamp: number;
    private isArchived: boolean;
    private items: ShoppingItemModel[] = [];

    constructor(name: string, timestamp: number = (new Date()).getTime(), isArchived: boolean = false) {
        super();
        this.name = name;
        this.timestamp = timestamp;
        this.isArchived = isArchived;
    }

    public addItem(item: ShoppingItemModel) {
        this.items.push(item);
        ShoppingListStore.getInstance().exportData(() => this.trigger(), (err) => {
            this.items.pop();
            Alert.alert("Error", err, [{ text: "OK" }]);
        });
    }

    public removeItem(item: ShoppingItemModel) {
        const index = this.items.indexOf(item);
        if (index > -1) {
            this.items.splice(index, 1);
        }
        ShoppingListStore.getInstance().exportData(() => this.trigger(), (err) => {
            this.items.splice(index, 0, item);
            Alert.alert("Error", err, [{ text: "OK" }]);
        });
    }

    public onItemChange = () => {
        ShoppingListStore.getInstance().exportData(() => this.trigger(), (err) => {
            Alert.alert("Error", err, [{ text: "OK" }]);
        });
    }

    setArchived(value: boolean) {
        if (this.isArchived !== value) {
            this.isArchived = value;
            ShoppingListStore.getInstance().onListsChanged();
        }
    }

    public remove() {
        ShoppingListStore.getInstance().removeList(this);
    }

    public importItems(items: ShoppingItemModel[]) {
        this.items = items;
    }

    public exportData() {
        return {
            name: this.name,
            timestamp: this.timestamp,
            isArchived: this.isArchived,
            items: this.items.map(item => item.exportData()),
        };
    }

    @autoSubscribe
    getNumberOfItems() {
        return this.items.length;
    }

    @autoSubscribe
    getNumberOfBoughtItems() {
        return this.items.filter(item => item.getBought()).length;
    }

    @autoSubscribe
    getItems() {
        return this.items.sort((a, b) => (a.getBought() === b.getBought()) ? 0 : a.getBought() ? 1 : -1).slice(0);
    }

    getArchived() {
        return this.isArchived;
    }

    getName() {
        return this.name;
    }

    getTimestamp() {
        return this.timestamp;
    }
}
