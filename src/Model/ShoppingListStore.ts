
import { Alert } from "react-native";
import { autoSubscribe, AutoSubscribeStore, StoreBase } from "resub";
import { FileManager } from "./FileManager";
import { ShoppingItemModel } from "./ShoppingItemModel";
import { ShoppingListModel } from "./ShoppingListModel";

@AutoSubscribeStore
export class ShoppingListStore extends StoreBase {
    private static instance: ShoppingListStore;
    private lists: ShoppingListModel[] = [];

    private listsLoaded: boolean = false;

    static getInstance() {
        if (!ShoppingListStore.instance) {
            ShoppingListStore.instance = new ShoppingListStore();
        }
        return ShoppingListStore.instance;
    }

    addList(name: string) {
        this.lists.push(new ShoppingListModel(name));
        this.exportData(() => this.trigger(),
            err => { this.lists.pop(); Alert.alert("Error", err, [{ text: "OK" }]); });
    }

    removeList(list: ShoppingListModel) {
        const index = this.lists.indexOf(list);
        if (index > -1) {
            this.lists.splice(index, 1);
        }
        ShoppingListStore.getInstance().exportData(() => this.trigger(), (err) => {
            this.lists.splice(index, 0, list);
            Alert.alert("Error", err, [{ text: "OK" }]);
        });
    }

    onListsChanged() {
        ShoppingListStore.getInstance().exportData(() => this.trigger(), (err) => {
            Alert.alert("Error", err, [{ text: "OK" }]);
        });
    }

    exportData(onSucces: () => void, onFailed: (err: any) => void) {
        const data = this.lists.map(list => list.exportData());
        FileManager.writeToFile(JSON.stringify(data)).then(() => {
            onSucces();
        }).catch(err => onFailed(err));
    }

    loadLists() {
        if (this.listsLoaded) {
            return;
        }
        FileManager.readDataFile().then((contents) => {
            if (!contents) {
                return;
            }
            try {
                const listData = JSON.parse(contents);
                for (const list of listData) {
                    const shoppingList = new ShoppingListModel(list.name, list.timestamp, list.isArchived);
                    const items = list.items.map(
                        (item) => new ShoppingItemModel(item.name, shoppingList, item.isBought));
                    shoppingList.importItems(items);
                    this.lists.push(shoppingList);
                }
                this.listsLoaded = true;
                this.trigger();
            } catch (err) {
                Alert.alert("Error", err, [{ text: "OK" }]);
            }
        }).catch((err) => Alert.alert("Error", err, [{ text: "OK" }]));
    }

    @autoSubscribe
    getLists(): ShoppingListModel[] {
        return this.lists;
    }

    @autoSubscribe
    getActiveLists(): ShoppingListModel[] {
        return this.lists.filter(list => !list.getArchived()).sort((a, b) => b.getTimestamp() - a.getTimestamp());
    }

    @autoSubscribe
    getArchivedLists(): ShoppingListModel[] {
        return this.lists.filter(list => list.getArchived()).sort((a, b) => b.getTimestamp() - a.getTimestamp());
    }
}
