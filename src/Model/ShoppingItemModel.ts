import { autoSubscribe, AutoSubscribeStore, StoreBase } from "resub";
import { ShoppingListModel } from "./ShoppingListModel";

@AutoSubscribeStore
export class ShoppingItemModel extends StoreBase {
    private name: string;
    private isBought: boolean;
    private timestamp: number;

    private parent: ShoppingListModel;

    constructor(name: string, parent: ShoppingListModel, isBought: boolean = false) {
        super();
        this.name = name;
        this.parent = parent;
        this.isBought = isBought;
        this.timestamp = (new Date()).getTime();
    }

    setIsBought(value: boolean) {
        if (this.isBought !== value) {
            this.isBought = value;
            this.parent.onItemChange();
            this.trigger();
        }
    }

    remove() {
        this.parent.removeItem(this);
    }

    exportData = () => {
        return {
            name: this.name,
            isBought: this.isBought,
        };
    }

    @autoSubscribe
    getBought(): boolean {
        return this.isBought;
    }

    getName(): string {
        return this.name;
    }

    getTimestamp(): number {
        return this.timestamp;
    }

    getParent(): ShoppingListModel {
        return this.parent;
    }
}
