import { ExternalDirectoryPath, readDir, readFile, writeFile } from "react-native-fs";

const FILE_NAME = "ShoppingLists.txt";
const FILE_PATH = `${ExternalDirectoryPath}/${FILE_NAME}`;

export class FileManager {

    static writeToFile(data: string): Promise<void> {
        return writeFile(FILE_PATH, data, "utf8");
    }

    static async readDataFile(): Promise<string> {
        const result = await readDir(ExternalDirectoryPath);
        if (result && result.find(item => item.name === FILE_NAME)) {
            return readFile(FILE_PATH, "utf8");
        }
        return null;
    }
}
