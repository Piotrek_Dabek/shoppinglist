import { createAppContainer, createStackNavigator } from "react-navigation";
import { Colors } from "./Resources/Colors";
import { ListContentScreen } from "./Screens/ListContentScreen";
import { ListsScreen } from "./Screens/ListsScreen";

const AppNavigator = createStackNavigator({
  ListsScreen: {
    screen: ListsScreen,
  },
  ListContentScreen: {
    screen: ListContentScreen,
  },
},
  {
    initialRouteName: "ListsScreen",
    initialRouteParams: {isArchived: false},
    defaultNavigationOptions: {
      headerTitleStyle: {
        fontSize: 20,
        color: Colors.text,
      },
      headerStyle: {
        backgroundColor: Colors.primary,
      },
      headerTintColor: Colors.text,
    },
  });

export default createAppContainer(AppNavigator);
