import { Fab, Icon } from "native-base";
import React from "react";
import { StatusBar, StyleSheet, View, ViewStyle } from "react-native";
import { Colors } from "../Resources/Colors";

interface ViewBaseProps {
    style?: ViewStyle;
    children?: JSX.Element[] | JSX.Element;
    onFABPressed?: () => void;
}

export class ViewBase extends React.Component<ViewBaseProps, {}> {
    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <StatusBar
                    backgroundColor={Colors.primaryDark}
                    barStyle="light-content"
                />
                {this.props.children}
                {this.props.onFABPressed ?
                    <Fab
                        containerStyle={{}}
                        style={{ backgroundColor: Colors.accent }}
                        position="bottomRight"
                        onPress={this.props.onFABPressed}>
                        <Icon name="add" />
                    </Fab> : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        padding: 20,
    },
});
