import { Card } from "native-base";
import React from "react";
import { ViewStyle } from "react-native";
import Modal from "react-native-modal";

interface ModalBaseProps {
    isVisible: boolean;
    onDismiss: () => void;
    cardStyle?: ViewStyle;
}

export class ModalBase extends React.Component<ModalBaseProps, {}> {
    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                avoidKeyboard={true}
                onBackButtonPress={this.props.onDismiss}
                onBackdropPress={this.props.onDismiss}>
                <Card style={[{ padding: 10 }, this.props.cardStyle]}>
                    {this.props.children}
                </Card>
            </Modal>
        );
    }
}
