import { Button, Form, Input, Item, Label } from "native-base";
import React from "react";
import { Alert, Text } from "react-native";
import { ShoppingItemModel } from "../Model/ShoppingItemModel";
import { ShoppingListModel } from "../Model/ShoppingListModel";
import { Colors } from "../Resources/Colors";
import { ModalBase } from "./ModalBase";

interface AddItemModalProps {
    isVisible: boolean;
    onDismiss: () => void;
    list: ShoppingListModel;
}

interface AddItemModalState {
    itemName: string;
}

export class AddItemModal extends React.Component<AddItemModalProps, AddItemModalState> {

    componentWillReceiveProps(nextProps: AddItemModalProps) {
        if (nextProps.isVisible === true && this.props.isVisible === false) {
            this.setState({ itemName: "" });
        }
    }

    render() {
        return (
            <ModalBase isVisible={this.props.isVisible} onDismiss={this.props.onDismiss}>
                <Form>
                    <Item stackedLabel last>
                        <Label style={{ color: Colors.textSecondary }}>Enter your item's name</Label>
                        <Input autoFocus={true} onChangeText={(value) => this.setState({ itemName: value })} />
                    </Item>
                </Form>
                <Button block style={{ backgroundColor: Colors.primary, marginTop: 20 }} onPress={this.onButtonPressed}>
                    <Text style={{ color: Colors.text, fontSize: 20, fontWeight: "bold" }}>Add</Text>
                </Button>
            </ModalBase>
        );
    }

    private onButtonPressed = () => {
        if (this.state.itemName.length) {
            this.props.list.addItem(new ShoppingItemModel(this.state.itemName, this.props.list));
            this.props.onDismiss();
        } else {
            Alert.alert("Empty", "Item name cannot be empty!", [{ text: "OK" }]);
        }
    }
}
