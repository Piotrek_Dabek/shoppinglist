import React, { Component } from "react";
import { findNodeHandle, NativeModules, StyleSheet, View, ViewStyle } from "react-native";
import { IconButton } from "./IconButton";

const UIManager = NativeModules.UIManager;

interface ToolbarDropdownProps {
    labels: string[];
    onPress: (index: number) => void;
    style?: ViewStyle;
}

export default class ToolbarDropdown extends Component<ToolbarDropdownProps> {
    private menu: any;

    render() {
        return (
            <View style={{ flexDirection: "row" }}>
                <View>
                    <View ref={c => this.menu = c}
                        style={{
                            backgroundColor: "transparent",
                            width: 1,
                            height: StyleSheet.hairlineWidth,
                        }}
                    />
                    <IconButton
                        onPress={() => this.onMenuPressed(this.props.labels)} icon={"more"} style={this.props.style} />
                </View>
            </View>
        );
    }

    onMenuPressed = (labels: string[]) => {
        const { onPress } = this.props;
        UIManager.showPopupMenu(
            findNodeHandle(this.menu),
            labels,
            () => {},
            (result, index) => {
                if (result === "dismissed") { return; }
                if (onPress) {
                    onPress(index);
                }
            },
        );
    }
}
