import React from "react";
import { Text, View, ViewStyle } from "react-native";
import { ComponentBase } from "resub";
import { ShoppingListModel } from "../Model/ShoppingListModel";
import { Colors } from "../Resources/Colors";
import ToolbarDropdown from "./ToolbarDropdown";
import { TouchableCard } from "./TouchableCard";

interface ListCardProps extends React.Props<any> {
    style?: ViewStyle;
    list: ShoppingListModel;
    navigation: any;
}

interface ListCardState {
    numberOfBoughtItems: number;
    numberOfItems: number;
}

export class ListCard extends ComponentBase<ListCardProps, ListCardState> {
    protected _buildState(props: ListCardProps) {
        return {
            numberOfBoughtItems: props.list.getNumberOfBoughtItems(),
            numberOfItems: props.list.getNumberOfItems(),
        };
    }

    render() {
        return (
            <TouchableCard onPress={this.onPress}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <View style={{ maxWidth: "70%" }}>
                        {this.renderName()}
                        {this.renderDate()}
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        {this.renderCounter()}
                        <ToolbarDropdown
                            labels={["Delete", this.props.list.getArchived() ? "Restore" : "Archive"]}
                            onPress={(index) => this.onDropdownPress(index)}
                            style={{ marginLeft: 10 }} />
                    </View>
                </View>
            </TouchableCard>
        );
    }

    private renderName() {
        return (
            <Text numberOfLines={1} style={{
                fontSize: 20,
                fontWeight: "bold",
                color: Colors.primaryText,
                marginBottom: 5,
            }}>{this.props.list.getName()}</Text>
        );
    }

    private renderDate() {
        return (
            <Text numberOfLines={1} style={{
                fontSize: 12,
                color: Colors.textSecondary,
            }}>{(new Date(this.props.list.getTimestamp())).toDateString()}</Text>
        );
    }

    private renderCounter() {
        return (
            <Text style={{
                fontSize: 20,
                fontWeight: "bold",
                color: Colors.primaryDark,
            }}>{`${this.state.numberOfBoughtItems}/${this.state.numberOfItems}`}</Text>
        );
    }

    private onPress = () => {
        this.props.navigation.navigate("ListContentScreen", { shoppingList: this.props.list });
    }

    private onDropdownPress = (index: number) => {
        switch (index) {
            case 0:
                this.props.list.remove();
                break;
            case 1:
                this.props.list.setArchived(!this.props.list.getArchived());
                break;
            default:
                break;
        }
    }
}
