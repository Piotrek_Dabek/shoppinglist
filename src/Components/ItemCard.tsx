import { CheckBox } from "native-base";
import React from "react";
import { Text, View, ViewStyle } from "react-native";
import { ComponentBase } from "resub";
import { ShoppingItemModel } from "../Model/ShoppingItemModel";
import { Colors } from "../Resources/Colors";
import ToolbarDropdown from "./ToolbarDropdown";
import { TouchableCard } from "./TouchableCard";

interface ItemCardProps extends React.Props<any> {
    style?: ViewStyle;
    item: ShoppingItemModel;
}

interface ItemCardState extends React.Props<any> {
    isBought: boolean;
}

export class ItemCard extends ComponentBase<ItemCardProps, ItemCardState> {
    protected _buildState(props: ItemCardProps) {
        return {
            isBought: props.item.getBought(),
        };
    }

    render() {
        return (
            <TouchableCard onPress={this.onPress} disabled={this.props.item.getParent().getArchived()}>
                <View style={{
                     marginVertical: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <CheckBox style={{ marginLeft: -10 }} checked={this.state.isBought} color={Colors.primaryDark}
                            onPress={this.onPress} disabled={this.props.item.getParent().getArchived()} />
                        {this.renderName()}
                    </View>
                    {this.props.item.getParent().getArchived() ? null :
                        <ToolbarDropdown
                            labels={["Delete"]}
                            onPress={(index) => this.onDropdownPress(index)} />}
                </View>
            </TouchableCard >
        );
    }

    private renderName() {
        return (
            <View style={{ marginLeft: 20 }}>
                <Text numberOfLines={1} style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: Colors.primaryText,
                }}>{this.props.item.getName()}</Text>
            </View>
        );
    }

    private onPress = () => {
        this.props.item.setIsBought(!this.state.isBought);
    }

    private onDropdownPress = (index: number) => {
        switch (index) {
            case 0:
                this.props.item.remove();
                break;
        }
    }
}
