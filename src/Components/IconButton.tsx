import { Icon } from "native-base";
import React from "react";
import { TouchableOpacity, ViewStyle } from "react-native";
import { Colors } from "../Resources/Colors";

interface IconButtonProps {
    onPress: () => void;
    icon: string;
    style?: ViewStyle;
}

export class IconButton extends React.Component<IconButtonProps, {}> {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}
                style={[{
                    backgroundColor: Colors.primaryDark,
                    height: 35,
                    width: 35,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 2,
                }, this.props.style]}>
                <Icon
                    name={this.props.icon}
                    fontSize={20}
                    style={{ color: Colors.text }}
                />
            </TouchableOpacity>
        );
    }
}
