import { Button, Form, Input, Item, Label } from "native-base";
import React from "react";
import { Alert, Text } from "react-native";
import { ShoppingListStore } from "../Model/ShoppingListStore";
import { Colors } from "../Resources/Colors";
import { ModalBase } from "./ModalBase";

interface AddListModalProps {
    isVisible: boolean;
    onDismiss: () => void;
}

interface AddListModalState {
    listName: string;
}

export class AddListModal extends React.Component<AddListModalProps, AddListModalState> {
    constructor(props) {
        super(props);
        this.state = {
            listName: "",
        };
    }

    componentWillReceiveProps(nextProps: AddListModalProps) {
        if (nextProps.isVisible === true && this.props.isVisible === false) {
            this.setState({ listName: "" });
        }
    }

    render() {
        return (
            <ModalBase isVisible={this.props.isVisible} onDismiss={this.props.onDismiss}>
                <Form>
                    <Item stackedLabel last>
                        <Label style={{ color: Colors.textSecondary }}>Enter your list's name</Label>
                        <Input autoFocus={true} onChangeText={(value) => this.setState({ listName: value })} />
                    </Item>
                </Form>
                <Button block style={{ backgroundColor: Colors.primary, marginTop: 20 }} onPress={this.onButtonPressed}>
                    <Text style={{ color: Colors.text, fontSize: 20, fontWeight: "bold" }}>Add</Text>
                </Button>
            </ModalBase>
        );
    }

    private onButtonPressed = () => {
        if (this.state.listName.length) {
            ShoppingListStore.getInstance().addList(this.state.listName);
            this.props.onDismiss();
        } else {
            Alert.alert("Empty", "List name cannot be empty!", [{ text: "OK" }]);
        }

    }
}
